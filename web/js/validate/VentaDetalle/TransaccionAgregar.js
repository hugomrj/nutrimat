
function VentaDetalle_TransaccionAgregar(){

    vdta_campos_interaccion();

    var vdta_agregar = document.getElementById('vdta_agregar');
    vdta_agregar.addEventListener('click',
        function() {
                
                if (VentaDetalle_TransaccionAgregar_validad() )                
                {           
                 
                    var form = document.getElementById("vdta_form");            
                    var accion =  form.getAttribute('action') ; 

                    AjaxPeticionURL( accion, getDataForm(form) );
                
                    VentaDetalle_TransaccionLista();
                    vdta_cerrar.click();                          
                }
        },
        false
    );
    
   
   
   
    var vdta_cerrar = document.getElementById('vdta_cerrar');
    vdta_cerrar.addEventListener('click',
        function() {    
            javascript:ocultarVentana('capa_oscura');
            javascript:ocultarVentana('capa_clara');
        },
        false
    );           
    

}




function vdta_campos_interaccion()
{
     
    var vdta_producto = document.getElementById( 'vdta_producto' );      
    if (vdta_producto.value == ""){
        vdta_producto.value = 0;
    }         
    vdta_producto.onfocus  = function() {                
        recibirEnfoque('vdta_i2');            
    };
    vdta_producto.onblur  = function() {
        perderEnfoque('vdta_i2');
        AjaxPeticion('../Producto/Mostrar/Nombre.do?valor='+this.value,  'vdta_producto_nombre') ;
        AjaxPeticionValue('../Producto/Mostrar/PrecioVenta.do?valor='+this.value,  'vdta_precio_venta') ;        
        AjaxPeticionValue('../Producto/Mostrar/Impuesto.do?valor='+this.value,  'vdta_impuesto') ;        
        
        zero( 'vdta_producto' );
        vdta_cantidad.focus();  
        
        document.getElementById( 'vdta_precio_venta' ).value = formatoNumero(document.getElementById( 'vdta_precio_venta' ).value);        
        
    };
    
    var vdta_qry_producto = document.getElementById( 'vdta_qry_producto');
    vdta_qry_producto.addEventListener('click',
        function() 
        {   
            mostrarVentana('capa_oscura_segunda');
            mostrarVentana('capa_clara_segunda');
            dimensionarVentana('capa_clara_segunda', 700, 500);
            Busqueda_relacionada("Producto", "vdta_" , "vdta_producto_nombre", "Nombre");      
        }, 
        false
    );            
    
    
    var vdta_cantidad = document.getElementById( 'vdta_cantidad');  
    if (vdta_cantidad.value == ""){
        vdta_cantidad.value = 0;
    }         
    vdta_cantidad.onfocus  = function() {                
        recibirEnfoque('vdta_i3');
        vdta_cantidad.select();                
    };
    vdta_cantidad.onblur  = function() {
        perderEnfoque('vdta_i3');        
        vdta_cantidad.onkeyup();
        vdta_sub_total.oncommand();
    };        
    vdta_cantidad.onkeyup  = function() {
        vdta_cantidad.value = formatoNumero(vdta_cantidad.value);
    };             
    
    
    
    var vdta_precio = document.getElementById( 'vdta_precio_venta');  
    if (vdta_precio.value == ""){
        vdta_precio.value = 0;
    }           
        
    
    
    
    var vdta_sub_total = document.getElementById( 'vdta_sub_total');
    if (vdta_sub_total.value == ""){
        vdta_sub_total.value = 0;
    }   
    vdta_sub_total.oncommand  = function() {
        vdta_sub_total.value = multiplicar(vdta_precio.value, vdta_cantidad.value);
        vdta_sub_total.value = formatoNumero(vdta_sub_total.value);
    };    
    

}



function VentaDetalle_TransaccionAgregar_validad(){
    
    
    var nombre = document.getElementById( "vdta_producto_nombre") ;
    if (nombre.value.trim()=='') 
    {
        alerta_error('Falta agregar producto');
        document.getElementById( 'vdta_producto' ).focus();
        //nombre.focus();
        return false;
    }  
    
    
    var cantidad = document.getElementById( "vdta_cantidad") ;
    if (cantidad.value.trim()=='' || (cantidad.value.trim()=='0') ) 
    {
        alerta_error('Falta agregar cantidad');
        cantidad.focus();
        return false;
    }          
    
        
    var precio = document.getElementById( "vdta_precio_venta") ;
    if (precio.value.trim()=='' || (precio.value.trim()=='0') ) 
    {
        alerta_error('Falta agregar precio');
        precio.focus();
        return false;
    }          
            
    return true;
    
}



