
window.onload = function() {
        
    MensajesOnload();    
    campos_interaccion();            
    VentaDetalle_TransaccionLista();
           
    var vn_guardar = document.getElementById('vn_guardar');
    vn_guardar.addEventListener('click',
        function() {
    
    
                if ( factura_nuevo_validar_form() )
                {    
                                
                    var form = document.getElementById("vn_form");            
                    var accion =  form.getAttribute('action') ; 

                    var postPie = getPost( "vdtl_", ["gravada0", "gravada5", "gravada10",
                                                                    "iva5", "iva10", "monto_total"] );

                    var numerofactura = "";                        
                    numerofactura = AjaxPeticionURL( accion, getDataForm(form) + postPie );                        


                    if ( MensajeErrorCheck() )
                    {
                        
                        // esta parte tal vez se tanga que desaparecer, 
                        // se le llamara a factura por jspx directo
                        window.location = "../Venta/FacturaMostrar.do?id="+numerofactura;    
                        
                        
                        
                    }
                    else
                    {
                        MensajesOnload();
                    }            
                        
                }                            
        },
        false
    );  

  
    var vn_cancelar = document.getElementById('vn_cancelar');
    vn_cancelar.addEventListener('click',
        function() {            
            window.location = "../Venta/Transaccion/Cancelar.do";        
        },
        false
    );  
                             
};




function campos_interaccion()
{      
    var vn_factura = document.getElementById( 'vn_factura' );        
    vn_factura.onfocus  = function() {                
        recibirEnfoque('i2');            
    };
    vn_factura.onblur  = function() {
        perderEnfoque('i2');
        vn_factura.onkeyup();
    };   
    
    /*
    vn_factura.onkeypress  = function() {
        return isNumberKey(event);
    };   
*/
    vn_factura.onkeyup = function() {
      vn_factura.value = formatoNumero(vn_factura.value);
    }


   
    
    var vn_fecha_factura = document.getElementById('vn_fecha_factura');        
    if (vn_fecha_factura.type == 'text')
    {        
        vn_fecha_factura.setAttribute('placeholder', '__/__/____' );        
    }
    vn_fecha_factura.onfocus  = function() {                
        recibirEnfoque('i3');            
    };
    vn_fecha_factura.onblur  = function() {
        perderEnfoque('i3');
    };
    
    
    
    var vn_cliente = document.getElementById( 'vn_cliente' );  
    if (vn_cliente.value == ""){
        vn_cliente.value = 0;
    }           
    vn_cliente.onfocus  = function() {                
        recibirEnfoque('i4');            
    };
    vn_cliente.onblur  = function() {
        perderEnfoque('i4');        
        AjaxPeticion('../Cliente/Mostrar/Nombre.do?valor='+this.value,  'vn_cliente_nombre') ;
        zero( 'vn_cliente' );
    };

    
    var vn_qry_cliente = document.getElementById( 'vn_qry_cliente');
    vn_qry_cliente.addEventListener('click',
        function() 
        {                   
            mostrarVentana('capa_oscura_segunda');
            mostrarVentana('capa_clara_segunda');
            dimensionarVentana('capa_clara_segunda', 700, 500);            
            Busqueda_relacionada("Cliente", "vn_" , "vn_cliente_nombre", "Nombre");                         
        }, 
        false
    );            
    

}




function factura_nuevo_validar_form()
{
    var vn_factura = document.getElementById( "vn_factura") ;
    if ((vn_factura.value.trim()=='') || (vn_factura.value.trim()=='0'))
    {
        alerta_error('Falta numero de factura');
        vn_factura.focus();
        return false;
    }

    // falta fecha
    var vn_fecha_factura = document.getElementById( "vn_fecha_factura");
    if (vn_fecha_factura.type == 'text')
    {
        if (vn_fecha_factura.value == "")
        {            
            alerta_error('La fecha de factura no puede estar vacia');     
            vn_fecha_factura.focus();
            return false;                
        }
        else
        {
            if (!(validaFechaDDMMAAAA(vn_fecha_factura.value)))
            {                
                alerta_error('No es una fecha valida');
                vn_fecha_factura.focus();
                return false;                
            } 
        }
    }
    if (vn_fecha_factura.type == 'date')
    {        
        if (vn_fecha_factura.value == "")
        {              
            alerta_error('La fecha de factura no puede estar vacia');            
            vn_fecha_factura.focus();
            return false;       
        }
    }    


    
    var vn_cliente = document.getElementById( "vn_cliente") ;
    if (vn_cliente.value.trim()=='' || (vn_cliente.value.trim()=='0') ) 
    {
        alerta_error('Falta agregar cliente');
        vn_cliente.focus();
        return false;
    }    



    // controla que exista sub total
    var vdtl_monto_total = document.getElementById( "vdtl_monto_total") ;    
    if (vdtl_monto_total.innerHTML.trim()=='' || (vdtl_monto_total.innerHTML.trim()=='0') ) 
    {
        alerta_error('Falta agregar productos'); 
        return false;
    }    


    return true;


}

