

window.onload = function() {
    
    var id_compra = getParametroValor("id");    
    var jsonResponse = AjaxUrl( "../Compra/Factura.json?id="+id_compra );
    
    if (jsonResponse != null)
    {
        
        var objetoJson = JSON.parse(jsonResponse);            
        
        document.getElementById('cf_compra').value = objetoJson.compra ;
        document.getElementById('cf_factura').value = objetoJson.factura ;
        
        document.getElementById('cf_fecha_factura').value = formatoJSONFecha( (objetoJson.fecha_factura) );  
        
        document.getElementById('cf_proveedor').value = objetoJson.proveedor;  
        
        document.getElementById('cf_proveedor_nombre').innerHTML = objetoJson.proveedor_nombre ;

        AjaxPeticion('../CompraDetalle/Collection.do?id='+id_compra,'tab_body');       

        fomato_tabla ();
        
        var cdl_suma_cantidad = document.getElementById('cdl_suma_cantidad');        
        cdl_suma_cantidad.innerHTML = formatoNumero(objetoJson.cantidad);    

        var cdl_suma_subtotal = document.getElementById('cdl_suma_subtotal');        
        cdl_suma_subtotal.innerHTML = formatoNumero(objetoJson.total);        
        
        MensajesOnload();    
        
    }
        
    
    var cf_imprimir = document.getElementById('cf_imprimir');
    cf_imprimir.addEventListener('click',
        function() {
            window.open('../Compra/FacturaReporte.do?codigo='+id_compra, '_blank');            
        },
        false
    );  
    
    
    var cf_salir = document.getElementById('cf_salir');
    cf_salir.addEventListener('click',
        function() {
            window.location = "../Compra/Listar.do";      
        },
        false
    );  
    
                             
};





function fomato_tabla (){
    
    var table = document.getElementById('cdl_tabla').getElementsByTagName('tbody')[0];
    var celda ;
    var valorcelda ;

    for ( i=0; i < table.rows.length; i++ )
    {        
        celda = document.getElementById("cdl_tabla").getElementsByTagName('tbody')[0].rows[i].cells[2] ;                
        valorcelda = celda.innerHTML;                
        celda.innerHTML = formatoNumero(valorcelda);
    
        celda = document.getElementById("cdl_tabla").getElementsByTagName('tbody')[0].rows[i].cells[3] ;        
        valorcelda = celda.innerHTML;                
        celda.innerHTML = formatoNumero(valorcelda);
    
        celda = document.getElementById("cdl_tabla").getElementsByTagName('tbody')[0].rows[i].cells[4] ;        
        valorcelda = celda.innerHTML;                
        celda.innerHTML = formatoNumero(valorcelda);

    }    

}


