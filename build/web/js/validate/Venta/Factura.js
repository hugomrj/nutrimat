

window.onload = function() {
    
    var id_venta = getParametroValor("id");
    var jsonResponse = AjaxUrl( "../Venta/Factura.json?id="+id_venta );
        
    if (jsonResponse != null)
    {
        
        var objetoJson = JSON.parse(jsonResponse);    

        var vf_venta = document.getElementById('vf_venta');
        vf_venta.value = objetoJson.venta ;

        var vf_factura = document.getElementById('vf_factura');
        vf_factura.value = objetoJson.factura ;

        var vf_fecha_factura = document.getElementById('vf_fecha_factura');    
        vf_fecha_factura.value = formatoJSONFecha( (objetoJson.fecha_factura) );          

        var vf_cliente = document.getElementById('vf_cliente');    
        vf_cliente.value = objetoJson.cliente.cliente ;

        var vf_cliente_nombre = document.getElementById('vf_cliente_nombre');    
        vf_cliente_nombre.innerHTML = objetoJson.cliente.nombre + " " + objetoJson.cliente.apellido ;

        AjaxPeticion('../VentaDetalle/Collection.do?id='+id_venta,'tab_body');      

        document.getElementById('vdcl_gravada0').innerHTML = formatoNumero(objetoJson.gravada0.toString());
        document.getElementById('vdcl_gravada5').innerHTML = formatoNumero(objetoJson.gravada5.toString());       
        
        document.getElementById('vdcl_gravada10').innerHTML =formatoNumero(objetoJson.gravada10.toString());
        
                        
        document.getElementById('vdcl_iva5').innerHTML =  formatoNumero(objetoJson.iva5.toString());
        document.getElementById('vdcl_iva10').innerHTML =  formatoNumero( objetoJson.iva10.toString()) ;
        document.getElementById('vdcl_IVA').innerHTML = formatoNumero( (objetoJson.iva5.valueOf() + objetoJson.iva10.valueOf()).toString() )  ;
        
        document.getElementById('vdcl_monto_total').innerHTML  = formatoNumero(objetoJson.monto_total.toString());
        AjaxPeticion ("../NumeroaLetras.do?numero=" +objetoJson.monto_total,  "vdcl_totalletra") ;


        fomato_tabla();        
        MensajesOnload();    
        
    }
    

    var vf_imprimirFactura = document.getElementById('vf_imprimirFactura');
    vf_imprimirFactura.addEventListener('click',
        function() {
            window.open('../Venta/FacturaReporte.do?codigo='+id_venta, '_blank');
        },
        false
    );  
    
        
    var vf_salir = document.getElementById('vf_salir');
    vf_salir.addEventListener('click',
        function() {
            window.location = "../Venta/Listar.do";      
        },
        false
    );  

    
};

 


function fomato_tabla (){

    var table = document.getElementById( "cdcl_tabla" ).getElementsByTagName('tbody')[0] ;
    var rows = table.rows.length;
    var cell ;
 
    for(i=0; i<rows; i++)
    {      
       
        cell = table.rows[i].cells[2] ;        
        cell.innerHTML = formatoNumero( (cell.innerHTML).trim() );  
                
        cell = table.rows[i].cells[3] ;                                  
        cell.innerHTML = formatoNumero(cell.innerHTML);        
          
        cell = table.rows[i].cells[4] ;                                  
        cell.innerHTML = formatoNumero(cell.innerHTML);        
       
        cell = table.rows[i].cells[5] ;                                  
        cell.innerHTML = formatoNumero(cell.innerHTML);              
    }    


}


