

function VentaDetalle_TransaccionLista(){

    AjaxPeticion('../VentaDetalle/Transaccion/Listar.do','tab_body');

    fomato_tabla();
    seleccionar_registro();

    var vdtl_agregar = document.getElementById('vdtl_agregar');
    vdtl_agregar.addEventListener('click',
        function() {
                mostrarVentana('capa_oscura');
                mostrarVentana('capa_clara');
                AjaxPeticion( '../VentaDetalle/Transaccion/Agregar.do' , 'capa_clara' );
                dimensionarVentana('capa_clara', 700, 250);

                VentaDetalle_TransaccionAgregar();
        },
        false
    );

    // sumas de subtotales
    // tal vez tenga que hacer una funcon distinta

    var vdtl_gravada0 = document.getElementById('vdtl_gravada0');
    AjaxPeticion ("../VentaDetalle/Suma/Impuesto0.do", "vdtl_gravada0") ;
    vdtl_gravada0.innerHTML = formatoNumero(vdtl_gravada0.innerHTML);


    var vdtl_gravada5 = document.getElementById('vdtl_gravada5');
    AjaxPeticion ("../VentaDetalle/Suma/Impuesto5.do", "vdtl_gravada5") ;
    vdtl_gravada5.innerHTML = formatoNumero(vdtl_gravada5.innerHTML);


    var vdtl_gravada10 = document.getElementById('vdtl_gravada10');
    AjaxPeticion ("../VentaDetalle/Suma/Impuesto10.do", "vdtl_gravada10") ;
    vdtl_gravada10.innerHTML = formatoNumero(vdtl_gravada10.innerHTML);


    var numeroletra = AjaxUrl("../VentaDetalle/Suma/Total.do");
    var vdtl_monto_total = document.getElementById('vdtl_monto_total');    
    vdtl_monto_total.innerHTML = numeroletra;
    vdtl_monto_total.innerHTML = formatoNumero(vdtl_monto_total.innerHTML);


    var vdtl_totalletra = document.getElementById('vdtl_totalletra');    
    AjaxPeticion ("../NumeroaLetras.do?numero=" +numeroletra,  "vdtl_totalletra") ;



    var vdtl_iva5 = document.getElementById('vdtl_iva5');
    AjaxPeticion ("../VentaDetalle/Gravada5.do", "vdtl_iva5") ;
    vdtl_gravada5.innerHTML = formatoNumero(vdtl_iva5.innerHTML);



 
    var vdtl_iva10 = document.getElementById('vdtl_iva10');
    AjaxPeticion ("../VentaDetalle/Gravada10.do", "vdtl_iva10") ;
    vdtl_iva10.innerHTML = formatoNumero(vdtl_iva10.innerHTML);   
   
   
    var vdtl_IVA = document.getElementById('vdtl_IVA');
    AjaxPeticion ("../VentaDetalle/GravadaIVA.do", "vdtl_IVA") ;
    vdtl_IVA.innerHTML = formatoNumero(vdtl_IVA.innerHTML);   
   
             


}


function fomato_tabla (){


//    var table = document.getElementById( "vdtl_tabla" ).getElementsByTagName('tbody')[0];
    var table = document.getElementById( "vdtl_tabla" );
    var rows = table.rows.length;
    var cell ;


    //for(i=1; i<rows; i++)
    for(i=2; i<rows; i++)
    {



        cell = table.rows[i].cells[2] ;
        cell.innerHTML = formatoNumero(cell.innerHTML);

        cell = table.rows[i].cells[3] ;
        cell.innerHTML = formatoNumero(cell.innerHTML);

        cell = table.rows[i].cells[4] ;
        cell.innerHTML = formatoNumero(cell.innerHTML);

        cell = table.rows[i].cells[5] ;
        cell.innerHTML = formatoNumero(cell.innerHTML);


    }







/*
    var table = document.getElementById('cdtl_tabla').getElementsByTagName('tbody')[0];


    var celda ;
    var valorcelda ;


    for ( i=0; i < table.rows.length; i++ )
    {



        var rows = document.getElementById("cdtl_tabla").getElementsByTagName('tbody')[0].rows[i] ;


        celda = rows.cells[2] ;
        valorcelda = celda.innerHTML;
        celda.innerHTML = formatoNumero(valorcelda);

        celda = document.getElementById("cdtl_tabla").getElementsByTagName('tbody')[0].rows[i].cells[3] ;
        valorcelda = celda.innerHTML;
        celda.innerHTML = formatoNumero(valorcelda);

        celda = document.getElementById("cdtl_tabla").getElementsByTagName('tbody')[0].rows[i].cells[4] ;
        valorcelda = celda.innerHTML;
        celda.innerHTML = formatoNumero(valorcelda);

    }

*/

}


    function seleccionar_registro()
    {
        var tabla_qry = document.getElementById('vdtl_tabla');
        var rows = tabla_qry.getElementsByTagName('tr');

        for (var i=0 ; i < rows.length; i++)
        {
            rows[i].addEventListener ( 'click',
                function() {

                    //registroid = this.getElementsByTagName( "producto" )[0].dataset.reg;
               //     registroid = this.getElementsByTagName( "id" )[0].dataset.reg;



                  /*

                        javascript:mostrarVentana('capa_oscura');
                        javascript:mostrarVentana('capa_clara');
                        AjaxPeticion( '../VentaDetalle/Transaccion/Registro.do?id='+registroid
                        , 'capa_clara' );
                        dimensionarVentana('capa_clara', 700, 250);

                        CompraDetalle_TransaccionRegistro();
                    */

                    },
                    false
                );

        }

    };
