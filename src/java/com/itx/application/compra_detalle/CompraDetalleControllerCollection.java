/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itx.application.compra_detalle;

import com.itx.application.compra.Compra;
import com.itx.application.venta_detalle.*;
import com.itx.application.venta.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.Secuencia;


@WebServlet(name = "CompraDetalleControllerCollection", 
        urlPatterns = {"/CompraDetalle/Collection.do"})


public class CompraDetalleControllerCollection extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        
        response.setContentType("text/html;charset=UTF-8");

        // obteber id y contruir las consultas 
        Integer factura = Integer.parseInt(request.getParameter("id")) ;         
        Compra compra = new Compra();
        Persistencia persistencia = new Persistencia();        
        compra = (Compra) persistencia.filtrarId(compra, factura);
                
        if (compra != null) {
        
            Secuencia<CompraDetalle> lista = new Secuencia<CompraDetalle>();           
            List<CompraDetalle> detalle = new ArrayList<CompraDetalle>();
            detalle = lista.listaColeccion(new CompraDetalle(), compra, "", 1);
            request.setAttribute("detalle", detalle );        
            
        }

        request.getRequestDispatcher("../CompraDetalle/Sub/CollectionLista.jspx").include(request, response);        


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CompraDetalleControllerCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CompraDetalleControllerCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}








