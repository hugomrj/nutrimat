/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itx.application.compra;


import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import nebuleuse.ORM.Conexion;
import nebuleuse.ORM.RegistroMap;

/**
 *
 * @author hugom_000
 */
public class CompraDAO  {
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;  
    
    public CompraDAO ( )  {
        conexion.conectar();
    }

  
        public ResultSet  ListaCompras  ( String strBuscar, Integer pagina )
            throws Exception {
            
                statement = conexion.getConexion().createStatement();               
               // resultset = statement.executeQuery( "SELECT * FROM compras" );     
                resultset = statement.executeQuery( 
                    " SELECT \n" +
                    "  compras.compra, numero_factura factura, "
                    + "fecha, fecha_factura, compras.proveedor, forma_pago, \n" +
                    "  proveedores.nombre proveedor_nombre, \n" +
                    "  sum(compras_detalle.sub_total) suma_sub_total\n" +
                    "FROM \n" +
                    "  compras, compras_detalle, proveedores\n" +
                    "WHERE \n" +
                    "  compras.compra = compras_detalle.compra AND\n" +
                    "  proveedores.proveedor = compras.proveedor\n" +
                    " GROUP BY \n" +
                    "  compras.compra, numero_factura, \n" +
                    "  fecha, fecha_factura, compras.proveedor, \n" +
                    "  forma_pago, nombre "                         
                );     
           
                
                return resultset;
             
    }    
        
        public HashMap  getCompra  ( Integer codigo )
            throws Exception {
            
                statement = conexion.getConexion().createStatement();    
                resultset = statement.executeQuery("  "
                        + "SELECT \n" +
                            "  compras.compra, \n" +
                            "  compras.numero_factura factura, \n" +
                            "  compras.fecha, \n" +
                            "  to_char(fecha_factura, 'Mon DD, YYYY') as fecha_factura, \n" +
                            "  compras.proveedor, \n" +
                            "  compras.forma_pago, \n" +
                            "  proveedores.nombre proveedor_nombre, \n" +
                            "  sum(compras_detalle.sub_total) total, \n" +
                            "  sum(compras_detalle.cantidad) cantidad\n" +
                            "FROM \n" +
                            "  public.compras, \n" +
                            "  public.compras_detalle, \n" +
                            "  public.proveedores\n" +
                            "WHERE \n" +
                            "  compras.compra = compras_detalle.compra AND\n" +
                            "  proveedores.proveedor = compras.proveedor\n" +
                            "  and compras.compra = " + codigo +" "+
                            " GROUP BY \n" +
                            " compras.compra, \n" +
                            "  compras.numero_factura, \n" +
                            "  compras.fecha, \n" +
                            "  compras.fecha_factura, \n" +
                            "  compras.proveedor, \n" +
                            "  compras.forma_pago, \n" +
                            "  proveedores.nombre\n" +
                            "  "
                        + "");     
                
                RegistroMap registro = new RegistroMap();
                return registro.getRegistro(resultset);
             
    }    
   
    
}
