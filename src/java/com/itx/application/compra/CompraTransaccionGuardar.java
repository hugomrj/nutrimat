/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itx.application.compra;

import com.itx.application.compra_detalle.CompraDetalle;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nebuleuse.GUI.web.Mensaje;
import nebuleuse.ORM.Persistencia;


@WebServlet(name = "CompraTransaccionGuardar",
        urlPatterns = {"/Compra/Transaccion/Guardar.do"})


public class CompraTransaccionGuardar extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();        
                            
            
            Compra compra = new Compra();
            List<CompraDetalle> coleccion = new ArrayList<CompraDetalle>();  
            Persistencia persistencia = new Persistencia();               
            
            
            String sessionDetalle = "CompraDetalleTransaccionSessionLista";                        
            Mensaje mensaje = new Mensaje();                        

            
            if (request.getSession().getAttribute(sessionDetalle) != null  )
            {   
                coleccion = (List<CompraDetalle>) request.getSession().getAttribute(sessionDetalle);
                
                if ( coleccion.size() > 0)
                {                
                    
                    try {
                        
                        
                        compra = (Compra) persistencia.extraerRegistro(request, compra);
                        compra = (Compra) persistencia.insert(compra, request);
                        
                        
                        for (CompraDetalle detalle : coleccion)
                        {
                            detalle.setCompra(compra);
                            detalle = (CompraDetalle) persistencia.insert(detalle);
                        }
                        request.getSession().setAttribute(sessionDetalle, null);
                        out.print(compra.getCompra());                        
                        
                        mensaje.info("comprasOk", request);
                        
                    } catch (Exception ex) {
                        System.out.println("---- Error");
//                        Logger.getLogger(CompraTransaccionGuardar.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    finally{
                        System.out.println("---- final");
                    }
                    
                }  
            }                
            else
            {
                // sale y no es 
                System.out.println("-- no existe detalle");
                //mensaje.error("comprasOk", request);
                
                
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CompraTransaccionGuardar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CompraTransaccionGuardar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}


















